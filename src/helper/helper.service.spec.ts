import { Test, TestingModule } from '@nestjs/testing';
import { HelperService } from './helper.service';

describe('HelperService', () => {
  let service: HelperService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HelperService],
    }).compile();

    service = module.get<HelperService>(HelperService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('parseBirthday', () => {
    it('should return a string', () => {
      const date = new Date();
      const result = service.parseBirthday(date);
      expect(typeof result).toBe('string');
    });

    it('should return a string in format DD/MM/YYYY', () => {
      const date = new Date();
      const result = service.parseBirthday(date);
      expect(result).toMatch(/\d{2}\/\d{2}\/\d{4}/);
    });
  })

  describe('parseHoroscope', () => {
    it('should return a string', () => {
      const date = new Date();
      const result = service.parseHoroscope(date);
      expect(typeof result).toBe('string');
    });
  });

  describe('parseZodiac', () => {
    it('should return a string', () => {
      const date = new Date();
      const result = service.parseZodiac(date);
      expect(typeof result).toBe('string');
    });
  });
});
