import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class HelperService {
  public parseBirthday(birthday: Date): string {
    const month = String(birthday.getMonth() + 1).padStart(2, '0');
    const day = String(birthday.getDate()).padStart(2, '0');
    const year = String(birthday.getFullYear()).padStart(4, '0');
    return `${month}/${day}/${year}`;
  }

  public parseHoroscope(birthday: Date): string {
    const inputMonth = String(birthday.getMonth() + 1).padStart(2, '0');
    const inputDay = String(birthday.getDate()).padStart(2, '0');
    const horoscopePath = path.join(__dirname, '..', 'constants', 'horoscope.json');
    const horoscopes = JSON.parse(fs.readFileSync(horoscopePath, 'utf8'));
    const matchingHoroscope = horoscopes.find(entry => {
      const [startMonth, startDay] = entry.start.split('/');
      const [endMonth, endDay] = entry.end.split('/');

      if (startMonth === endMonth) {
        return (inputMonth === startMonth && (inputDay >= startDay && inputDay <= endDay));
      } else if (inputMonth === startMonth) {
        return inputDay >= startDay;
      } else if (inputMonth === endMonth) {
        return inputDay <= endDay;
      } else {
        return (inputMonth > startMonth && inputMonth < endMonth);
      }
    });
    return matchingHoroscope ? matchingHoroscope.name : "";
  }

  public parseZodiac(birthday: Date): string {
    const inputMonth = String(birthday.getMonth() + 1).padStart(2, '0');
    const inputDay = String(birthday.getDate()).padStart(2, '0');
    const zodiacPath = path.join(__dirname, '..', 'constants', 'zodiac.json');
    const zodiacs = JSON.parse(fs.readFileSync(zodiacPath, 'utf8'));
    const matchingZodiac = zodiacs.find(entry => {
      const [startMonth, startDay] = entry.start_date.split('/');
      const [endMonth, endDay] = entry.end_date.split('/');

      if (startMonth === endMonth) {
        return (inputMonth === startMonth && (inputDay >= startDay && inputDay <= endDay));
      } else if (inputMonth === startMonth) {
        return inputDay >= startDay;
      } else if (inputMonth === endMonth) {
        return inputDay <= endDay;
      } else {
        return (inputMonth > startMonth && inputMonth < endMonth);
      }
    });

    return matchingZodiac ? matchingZodiac.zodiac : "";
  }
}
