import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import { getModelToken } from '@nestjs/mongoose';
import { Users } from '../schemas';
import { LoginDto, RegisterDto } from './dto';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import * as bcrypt from 'bcrypt';

class JestUsersModel {
  constructor(private data) { }
  save = jest.fn().mockResolvedValue(this.data);
  static exists = jest.fn().mockResolvedValue(false);
  static create = jest.fn().mockResolvedValue(new JestUsersModel({}));
  static findOne = jest.fn().mockResolvedValue(new JestUsersModel({}));
}

describe('AuthService', () => {
  let authService: AuthService;
  let jestUsersModel: typeof JestUsersModel;
  let jwtService: JwtService;
  let configService: ConfigService;

  const mockJwtService = {
    sign: jest.fn(),
  };

  const mockConfigService = {
    get: jest.fn(),
  };
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: getModelToken(Users.name),
          useValue: JestUsersModel,
        },
        {
          provide: JwtService,
          useValue: mockJwtService,
        },
        {
          provide: ConfigService,
          useValue: mockConfigService,
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    jestUsersModel = module.get<typeof JestUsersModel>(getModelToken(Users.name));
    jwtService = module.get<JwtService>(JwtService);
    configService = module.get<ConfigService>(ConfigService);
  });

  describe('register', () => {
    beforeEach(() => {
      jestUsersModel.create.mockClear();
      jestUsersModel.exists.mockClear();
    });

    it('should register a new user', async () => {
      const registerDto: RegisterDto = {
        username: 'newuser',
        email: 'newuser@example.com',
        password: 'password123',
      };

      jestUsersModel.exists.mockResolvedValue(false);

      const hashedPassword = await bcrypt.hash(registerDto.password, 10);
      const newUser = {
        _id: 'mockedUserId',
        username: registerDto.username,
        email: registerDto.email,
        password: hashedPassword,
        save: jest.fn().mockResolvedValue(registerDto)
      };
      jestUsersModel.create.mockReturnValue(newUser);

      const result = await authService.register(registerDto);

      expect(result).toEqual({ message: 'User has been created successfully' });
      expect(jestUsersModel.exists).toHaveBeenCalledWith({
        $or: [{ username: registerDto.username }, { email: registerDto.email }],
      });
    });

    it('should return error if username or email already exists', async () => {
      const registerDto: RegisterDto = {
        username: 'existinguser',
        email: 'existinguser@example.com',
        password: 'password123',
      };

      jestUsersModel.exists.mockResolvedValue(true);

      const result = await authService.register(registerDto);

      expect(result).toEqual({ message: 'Username or email already exists' });
      expect(jestUsersModel.exists).toHaveBeenCalledWith({
        $or: [{ username: registerDto.username }, { email: registerDto.email }],
      });
      expect(jestUsersModel.create).not.toHaveBeenCalled();
    });
  });

  describe('login', () => {
    beforeEach(() => {
      jestUsersModel.findOne.mockClear();
      mockJwtService.sign.mockClear();
    });

    it('should log in a user with correct credentials', async () => {
      const loginDto: LoginDto = {
        username: 'existinguser',
        password: 'password123',
      };

      const hashedPassword = bcrypt.hashSync('password123', 10); // Hashed password
      jestUsersModel.findOne.mockResolvedValue({
        _id: 'mockedUserId',
        username: 'existinguser',
        email: 'existinguser@example.com',
        password: hashedPassword,
      });
      mockJwtService.sign.mockReturnValue('mockToken');
      const result = await authService.login(loginDto);

      expect(result).toEqual({
        message: 'User has been logged in successfully',
        access_token: expect.any(String),
      });
      expect(mockJwtService.sign).toHaveBeenCalled();
    });

    it('should return error if user is not found', async () => {
      const loginDto: LoginDto = {
        username: 'nonexistentuser',
        password: 'password123',
      };

      jestUsersModel.findOne.mockReturnValue(null);

      const result = await authService.login(loginDto);

      expect(result).toEqual({ message: 'User not found' });
      expect(jestUsersModel.findOne).toHaveBeenCalled();
      expect(mockJwtService.sign).not.toHaveBeenCalled();
    });

    it('should return error if username or email is not provided', async () => {
      const loginDto: LoginDto = {
        password: 'password123',
      };

      const result = await authService.login(loginDto);

      expect(result).toEqual({ message: 'Email or username is required' });
      expect(jestUsersModel.findOne).not.toHaveBeenCalled();
      expect(mockJwtService.sign).not.toHaveBeenCalled();
    });

  });
});
