import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Users } from '../schemas';
import { RegisterDto, LoginDto } from './dto';
import * as bcrypt from 'bcrypt';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(Users.name) private usersModel: Model<Users>,
    private config: ConfigService,
    private jwt: JwtService,
  ) { }

  async register(registerDto: RegisterDto) {
    const isExist = await this.usersModel.exists({
      $or: [{ username: registerDto.username }, { email: registerDto.email }]
    });
    if (isExist) return { message: "Username or email already exists" };

    const hashedPassword = await bcrypt.hash(registerDto.password, 10);
    const newUser = new this.usersModel({
      username: registerDto.username,
      email: registerDto.email,
      password: hashedPassword
    });

    try {
      await newUser.save();
      return { message: "User has been created successfully" };
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async login(loginDto: LoginDto) {
    const { username, email, password } = loginDto;
    if (!username && !email) return { message: "Email or username is required" };

    const user = await this.usersModel.findOne({
      $or: [{ username }, { email }]
    });
    if (!user) return { message: "User not found" };

    const isPasswordMatch = await bcrypt.compare(password, user.password);
    if (!isPasswordMatch) return { message: "Incorrect password" };

    const payload = { id: user._id.toString(), username: user.username, email: user.email };
    const secret = this.config.get('JWT_SECRET');
    const expiresIn = this.config.get('JWT_EXPIRED');
    const token = this.jwt.sign(payload, { secret: secret, expiresIn: expiresIn });

    return {
      "message": "User has been logged in successfully",
      "access_token": token
    }
  }
}
