import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { Users } from '../schemas';
import { getModelToken } from '@nestjs/mongoose';
import { LoginDto, RegisterDto } from './dto';

class MockAuthService {
  register = jest.fn();
  login = jest.fn();
}

describe('AuthController', () => {
  let controller: AuthController;
  let authService: MockAuthService;

  beforeEach(async () => {
    authService = new MockAuthService();

    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        {
          provide: getModelToken(Users.name),
          useValue: {
            findOne: jest.fn(),
            exists: jest.fn(),
          },
        },
        {
          provide: AuthService,
          useValue: authService,
        }
      ],
      imports: []
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('register', () => {
    it('should register a new user', async () => {
      const registerDto: RegisterDto = {
        username: 'newuser',
        email: 'newuser@example.com',
        password: 'password123',
      };

      authService.register.mockResolvedValue({ message: 'User has been created successfully' });

      const result = await controller.register(registerDto);

      expect(result).toEqual({ message: 'User has been created successfully' });
      expect(authService.register).toHaveBeenCalledWith(registerDto);
    });

  });

  describe('login', () => {
    it('should log in a user', async () => {
      const loginDto: LoginDto = {
        username: 'existinguser',
        password: 'password123',
      };

      authService.login.mockResolvedValue({
        message: 'User has been logged in successfully',
        access_token: 'mockedAccessToken',
      });

      const result = await controller.login(loginDto);

      expect(result).toEqual({
        message: 'User has been logged in successfully',
        access_token: 'mockedAccessToken',
      });
      expect(authService.login).toHaveBeenCalledWith(loginDto);
    });
  });
});
