import { IsEmail, IsNotEmpty, IsString, MinLength, MaxLength, IsAlphanumeric } from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class LoginDto {
  @IsString()
  // @IsNotEmpty()
  // @IsAlphanumeric()
  @ApiProperty()
  username?: string;

  @IsString()
  // @IsEmail()
  // @IsNotEmpty()
  @ApiProperty()
  email?: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(32)
  @IsAlphanumeric()
  @ApiProperty()
  password: string;
}