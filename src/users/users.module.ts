import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { Users, UsersSchema } from '../schemas';
import { HelperModule } from 'src/helper/helper.module';

@Module({
  imports: [MongooseModule.forFeature([{ name: Users.name, schema: UsersSchema }]), HelperModule],
  providers: [UsersService],
  controllers: [UsersController]
})
export class UsersModule { }
