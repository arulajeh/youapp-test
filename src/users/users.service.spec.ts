import { Test, TestingModule } from '@nestjs/testing';
import { UsersService } from './users.service';
import { getModelToken } from '@nestjs/mongoose';
import { Users } from '../schemas';
import { HelperService } from '../helper/helper.service';
import { InternalServerErrorException } from '@nestjs/common';
import { CreateUserDto, UpdateUserDto } from './dto';

class JestUserModel {
  constructor(private data) { }
  static findById = jest.fn();
  static findOneAndUpdate = jest.fn();
}

class MockHelperService {
  parseHoroscope = jest.fn();
  parseZodiac = jest.fn();
  parseBirthday = jest.fn();
}

describe('UsersService', () => {
  let usersService: UsersService;
  let mockUserModel: typeof JestUserModel;

  const mockHelperService = {
    parseHoroscope: jest.fn(),
    parseZodiac: jest.fn(),
    parseBirthday: jest.fn(),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        {
          provide: getModelToken(Users.name),
          useValue: JestUserModel,
        },
        {
          provide: HelperService,
          useValue: mockHelperService,
        },
      ],
    }).compile();

    usersService = module.get<UsersService>(UsersService);
    mockUserModel = module.get<typeof JestUserModel>(getModelToken(Users.name));
  });

  describe('createProfile', () => {
    beforeEach(() => {
      mockUserModel.findById.mockClear();
      mockUserModel.findOneAndUpdate.mockClear();
      mockHelperService.parseHoroscope.mockClear();
      mockHelperService.parseZodiac.mockClear();
      mockHelperService.parseBirthday.mockClear();
    })

    it('should create a new profile', async () => {
      const createDto: CreateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      const mockUser = {
        _id: 'mockedUserId',
        email: 'test@email.com',
        username: 'testuser',
      }
      const mockHoroscope = 'Mocked Horoscope';
      const mockZodiac = 'Mocked Zodiac';

      // Mocking findById method
      mockUserModel.findById.mockResolvedValue(mockUser);
      mockUserModel.findOneAndUpdate.mockResolvedValue({
        ...mockUser, ...createDto, horoscope: mockHoroscope, zodiac: mockZodiac
      })

      // Mocking helper service methods
      mockHelperService.parseHoroscope.mockReturnValue(mockHoroscope);
      mockHelperService.parseZodiac.mockReturnValue(mockZodiac);
      mockHelperService.parseBirthday.mockReturnValue('Mocked Birthday');

      const result = await usersService.createProfile('mockedUserId', createDto);

      expect(result).toEqual({
        message: 'Profile has been created successfully',
        data: {
          name: createDto.name,
          bio: createDto.bio,
          birthday: 'Mocked Birthday',
          username: mockUser.username,
          email: mockUser.email,
          gender: createDto.gender,
          horoscope: mockHoroscope,
          zodiac: mockZodiac,
          weight: createDto.weight,
          height: createDto.height,
          profilePicture: undefined,
          interests: createDto.interests,
        },
      });

      expect(mockHelperService.parseHoroscope).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseZodiac).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseBirthday).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockUserModel.findById).toHaveBeenCalledWith('mockedUserId');
      expect(mockUserModel.findOneAndUpdate).toHaveBeenCalledWith(
        { _id: 'mockedUserId' },
        expect.objectContaining(createDto),
        { upsert: true, new: true }
      );
    });

    it('should return error if user is not found', async () => {
      const createDto: CreateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      // Mocking findById method
      mockUserModel.findById.mockReturnValue(null);

      const result = await usersService.createProfile('nonexistentUserId', createDto);

      expect(result).toEqual({ message: 'User not found' });
      expect(mockUserModel.findById).toHaveBeenCalledWith('nonexistentUserId');
    });

    it('should return error if an error occurs during profile creation', async () => {
      const createDto: CreateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      const mockUser = {
        _id: 'mockedUserId',
        email: 'test@email.com',
        username: 'testuser',
      }
      // Mocking helper service methods
      mockHelperService.parseHoroscope.mockReturnValue('Mocked Horoscope');
      mockHelperService.parseZodiac.mockReturnValue('Mocked Zodiac');
      mockHelperService.parseBirthday.mockReturnValue('Mocked Birthday');

      // Mocking findById and findOneAndUpdate methods
      mockUserModel.findById.mockReturnValue(mockUser);
      mockUserModel.findOneAndUpdate.mockRejectedValue(new Error('Database error'));

      await expect(usersService.createProfile('mockedUserId', createDto)).rejects.toThrow(
        InternalServerErrorException
      );

      expect(mockHelperService.parseHoroscope).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseZodiac).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseBirthday).not.toHaveBeenCalled();
      expect(mockUserModel.findById).toHaveBeenCalledWith('mockedUserId');
      expect(mockUserModel.findOneAndUpdate).toHaveBeenCalledWith(
        { _id: 'mockedUserId' },
        expect.any(Object),
        { upsert: true, new: true }
      );
    });
  });

  describe('updateProfile', () => {

    beforeEach(() => {
      mockUserModel.findById.mockClear();
      mockUserModel.findOneAndUpdate.mockClear();
      mockHelperService.parseHoroscope.mockClear();
      mockHelperService.parseZodiac.mockClear();
      mockHelperService.parseBirthday.mockClear();
    })

    it('should update the profile', async () => {
      const createDto: UpdateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      const mockUser = {
        _id: 'mockedUserId',
        email: 'test@email.com',
        username: 'testuser',
      }
      const mockHoroscope = 'Mocked Horoscope';
      const mockZodiac = 'Mocked Zodiac';

      // Mocking findById method
      mockUserModel.findById.mockResolvedValue(mockUser);
      mockUserModel.findOneAndUpdate.mockResolvedValue({
        ...mockUser, ...createDto, horoscope: mockHoroscope, zodiac: mockZodiac
      })

      // Mocking helper service methods
      mockHelperService.parseHoroscope.mockReturnValue(mockHoroscope);
      mockHelperService.parseZodiac.mockReturnValue(mockZodiac);
      mockHelperService.parseBirthday.mockReturnValue('Mocked Birthday');

      const result = await usersService.updateProfile('mockedUserId', createDto);

      expect(result).toEqual({
        message: 'Profile has been updated successfully',
        data: {
          name: createDto.name,
          bio: createDto.bio,
          birthday: 'Mocked Birthday',
          username: mockUser.username,
          email: mockUser.email,
          gender: createDto.gender,
          horoscope: mockHoroscope,
          zodiac: mockZodiac,
          weight: createDto.weight,
          height: createDto.height,
          profilePicture: undefined,
          interests: createDto.interests,
        },
      });

      expect(mockHelperService.parseHoroscope).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseZodiac).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseBirthday).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockUserModel.findById).toHaveBeenCalledWith('mockedUserId');
      expect(mockUserModel.findOneAndUpdate).toHaveBeenCalledWith(
        { _id: 'mockedUserId' },
        expect.objectContaining(createDto),
        { upsert: true, new: true }
      );
    });

    it('should return error if profile is not found', async () => {
      const createDto: UpdateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      // Mocking findById method
      mockUserModel.findById.mockReturnValue(null);

      const result = await usersService.updateProfile('nonexistentUserId', createDto);

      expect(result).toEqual({ message: 'Profile not found' });
      expect(mockUserModel.findById).toHaveBeenCalledWith('nonexistentUserId');
    });

    it('should return error if an error occurs during profile update', async () => {
      const createDto: UpdateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      const mockUser = {
        _id: 'mockedUserId',
        email: 'test@email.com',
        username: 'testuser',
      }
      // Mocking helper service methods
      mockHelperService.parseHoroscope.mockReturnValue('Mocked Horoscope');
      mockHelperService.parseZodiac.mockReturnValue('Mocked Zodiac');
      mockHelperService.parseBirthday.mockReturnValue('Mocked Birthday');

      // Mocking findById and findOneAndUpdate methods
      mockUserModel.findById.mockReturnValue(mockUser);
      mockUserModel.findOneAndUpdate.mockRejectedValue(new Error('Database error'));

      await expect(usersService.updateProfile('mockedUserId', createDto)).rejects.toThrow(
        InternalServerErrorException
      );

      expect(mockHelperService.parseHoroscope).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseZodiac).toHaveBeenCalledWith(new Date(createDto.birthday));
      expect(mockHelperService.parseBirthday).not.toHaveBeenCalled();
      expect(mockUserModel.findById).toHaveBeenCalledWith('mockedUserId');
      expect(mockUserModel.findOneAndUpdate).toHaveBeenCalledWith(
        { _id: 'mockedUserId' },
        expect.any(Object),
        { upsert: true, new: true }
      );
    });
  });

  describe('getProfile', () => {
    beforeEach(() => {
      mockUserModel.findById.mockClear();
    })

    it('should get the profile', async () => {
      mockUserModel.findById.mockReturnValue({ name: 'Test User' })
      const result = await usersService.getProfile('mockedUserId');

      expect(result.message).toEqual("Profile has been found successfully");
      expect(mockUserModel.findById).toHaveBeenCalled();
      expect(result.data).toEqual(expect.any(Object));
    });

    it('should return error if profile is not found', async () => {
      mockUserModel.findById.mockReturnValue(null);

      const result = await usersService.getProfile('nonexistentUserId');

      expect(result).toEqual({ message: 'User not found' });
      expect(mockUserModel.findById).toHaveBeenCalled();
    });
  });
});
