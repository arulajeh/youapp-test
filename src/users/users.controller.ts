import { Controller, Get, Post, Put, UseGuards, Req, Body, HttpCode, UseInterceptors, UploadedFile } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from './users.service';
import { CreateUserDto, UpdateUserDto } from './dto';

@ApiTags('Users')
@Controller('api')
export class UsersController {
  constructor(
    private usersService: UsersService
  ) { }

  @UseGuards(AuthGuard('jwt'))
  @Post('/createProfile')
  @HttpCode(200)
  public async createProfile(@Req() req: any, @Body() body: CreateUserDto) {
    return this.usersService.createProfile(req.user.id, body);
  }

  @UseGuards(AuthGuard('jwt'))
  @Put('/updateProfile')
  @HttpCode(200)
  public async updateProfile(
    @Req() req: any,
    @Body() body: UpdateUserDto) {
    return this.usersService.updateProfile(req.user.id, body);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('/getProfile')
  @HttpCode(200)
  public async getProfile(@Req() req: any) {
    return this.usersService.getProfile(req.user.id);
  }
}
