import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Users } from '../schemas';
import { CreateUserDto, UpdateUserDto } from './dto';
import { HelperService } from '../helper/helper.service';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(Users.name) private usersModel: Model<Users>,
    private helper: HelperService
  ) { }

  async createProfile(id: string, createDto: CreateUserDto) {
    const isExist = await this.usersModel.findById(id);
    if (!isExist) return { message: "User not found" };
    try {
      const horoscope = this.helper.parseHoroscope(new Date(createDto.birthday));
      const zodiac = this.helper.parseZodiac(new Date(createDto.birthday));
      const profile = await this.usersModel.findOneAndUpdate({ _id: id },
        { ...createDto, horoscope, zodiac },
        { upsert: true, new: true });
      return {
        message: "Profile has been created successfully", data: {
          name: profile.name,
          bio: profile.bio,
          birthday: this.helper.parseBirthday(new Date(profile.birthday)),
          username: profile.username,
          email: profile.email,
          gender: profile.gender,
          horoscope: profile.horoscope,
          zodiac: profile.zodiac,
          weight: profile.weight,
          height: profile.height,
          profilePicture: profile.profilePicture,
          interests: profile.interests
        }
      };
    } catch (error) {
      throw new InternalServerErrorException("Something went wrong");
    }
  }

  async updateProfile(id: string, updateDto: UpdateUserDto) {
    const isExist = await this.usersModel.findById(id);
    if (!isExist) return { message: "Profile not found" };

    try {
      const horoscope = this.helper.parseHoroscope(new Date(updateDto.birthday));
      const zodiac = this.helper.parseZodiac(new Date(updateDto.birthday));
      const profile = await this.usersModel.findOneAndUpdate({ _id: id },
        { ...updateDto, horoscope, zodiac },
        { upsert: true, new: true });
      return {
        message: "Profile has been updated successfully", data: {
          name: profile.name,
          bio: profile.bio,
          birthday: this.helper.parseBirthday(new Date(profile.birthday)),
          username: profile.username,
          email: profile.email,
          gender: profile.gender,
          horoscope: profile.horoscope,
          zodiac: profile.zodiac,
          weight: profile.weight,
          height: profile.height,
          profilePicture: profile.profilePicture,
          interests: profile.interests
        }
      };
    } catch (error) {
      throw new InternalServerErrorException("Something went wrong");
    }
  }

  async getProfile(id: string) {
    const user = await this.usersModel.findById(id, { password: 0 });
    if (!user) return { message: "User not found" };
    return {
      message: "Profile has been found successfully",
      data: {
        name: user.name,
        bio: user.bio,
        birthday: user.birthday,
        username: user.username,
        email: user.email,
        gender: user.gender,
        horoscope: user.horoscope,
        zodiac: user.zodiac,
        weight: user.weight,
        height: user.height,
        profilePicture: user.profilePicture,
        interests: user.interests
      }
    };
  }
}
