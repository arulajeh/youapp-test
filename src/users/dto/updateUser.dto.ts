import { IsNotEmpty, IsString, IsAlphanumeric, IsDateString, IsNumber, IsArray } from "class-validator";
import { ApiProperty } from '@nestjs/swagger';

export class UpdateUserDto {
  @IsString()
  @IsNotEmpty()
  @IsAlphanumeric()
  @ApiProperty()
  name: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  birthday: string;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  height: number;

  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  weight: number;

  @IsArray()
  @IsString({ each: true })
  @IsNotEmpty()
  @ApiProperty()
  interests: string[];

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  bio: string;

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  gender: string;
}