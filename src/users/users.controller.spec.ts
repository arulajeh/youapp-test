import { Test, TestingModule } from '@nestjs/testing';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';
import { HelperService } from '../helper/helper.service';
import { CreateUserDto } from './dto';

class MockUsersService {
  createProfile = jest.fn();
  getProfile = jest.fn();
  updateProfile = jest.fn();
}

describe('UsersController', () => {
  let controller: UsersController;
  let usersService: MockUsersService;

  beforeEach(async () => {
    usersService = new MockUsersService();
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        HelperService,
        {
          provide: UsersService,
          useValue: usersService
        },
      ],
    }).compile();

    controller = module.get<UsersController>(UsersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  describe('createProfile', () => {
    it('should create a new profile', async () => {
      const createDto: CreateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      const mockReq = {
        user: { id: "mockedUserId" },
      };
      const mockResponse = {
        message: "Profile has been created successfully",
        data: {
          ...createDto,
        }
      }

      usersService.createProfile.mockReturnValue(mockResponse);
      const result = await controller.createProfile(mockReq, createDto);
      expect(result).toEqual({
        message: 'Profile has been created successfully',
        data: expect.any(Object),
      });
      expect(usersService.createProfile).toHaveBeenCalledWith(mockReq.user.id, createDto);
    })
  });

  describe('updateProfile', () => {
    it('should update profile', async () => {
      const createDto: CreateUserDto = {
        name: 'Test User',
        birthday: '2000-01-01',
        height: 170,
        weight: 65,
        interests: ['Reading', 'Swimming'],
        bio: 'Test bio',
        gender: 'male'
      };
      const mockReq = {
        user: { id: "mockedUserId" },
      };
      const mockResponse = {
        message: "Profile has been created successfully",
        data: {
          ...createDto,
        }
      }

      usersService.updateProfile.mockReturnValue(mockResponse);
      const result = await controller.updateProfile(mockReq, createDto);
      expect(result).toEqual({
        message: 'Profile has been created successfully',
        data: expect.any(Object),
      });
      expect(usersService.updateProfile).toHaveBeenCalledWith(mockReq.user.id, createDto);
    })
  })

  describe('getProfile', () => {
    it('should get profile', async () => {
      const mockReq = {
        user: { id: "mockedUserId" },
      };
      const mockResponse = {
        message: "Profile has been found successfully",
        data: {}
      }

      usersService.getProfile.mockReturnValue(mockResponse);
      const result = await controller.getProfile(mockReq);
      expect(result).toEqual({
        message: 'Profile has been found successfully',
        data: expect.any(Object),
      });
      expect(usersService.getProfile).toHaveBeenCalledWith(mockReq.user.id);
    })
  });
});
